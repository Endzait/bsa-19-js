import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const result = await callApi(endpoint, "GET");
      return JSON.parse(atob(result.content));
    } catch (error) {
      throw error;
    }
  }

    fight(fighter_one, fighter_two) {
        const firstFighter = fighter_one;
        const secondFighter = fighter_two;
        const hitPower = firstFighter.getHitPower();
        const blockPower = secondFighter.getBlockPower();

        const damage = +Math.abs(hitPower - blockPower);
        const healthLeft =
            secondFighter.getHealth() - damage > 0
                ? secondFighter.getHealth() - damage
                : 0;
        if (healthLeft === 0) {
            alert(`${firstFighter.name} won!`);
            window.location.reload();
        } else {
            secondFighter.setHealth(healthLeft);
            return healthLeft;
        }
    }
}

export const fighterService = new FighterService();
