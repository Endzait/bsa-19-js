import View from "./view";

class Modal extends View {
    constructor(fighter, handleClick) {
        super();
        this.createModal(fighter, handleClick);
        this.closeWrapper = this.closeWrapper.bind(this);
    }

    createModal(fighter, handleClick) {
        const { _id, name, health, attack, defense } = fighter;
        const nameElement = this.createName(name);
        this.element = this.createElement({ tagName: "div", className: "modal" });
        const modalContent = this.createElement({
            tagName: "div",
            className: "modal-content"
        });
        const closeModal = this.createElement({
            tagName: "div",
            className: "close"
        });
        const buttonElement = this.createButton("Choose");
        closeModal.innerHTML = "exit";
        const healthElement = this.createInput("Health", _id, health);
        const attackElement = this.createInput("Attack", _id, attack);
        const defenseElement = this.createInput("Defense", _id, defense);

        modalContent.append(
            closeModal,
            nameElement,
            healthElement,
            attackElement,
            defenseElement,
            buttonElement
        );
        this.element.append(modalContent);
        let buttonListener;
        let closeListener;
        buttonListener = buttonElement.addEventListener("click", event => {
            buttonElement.removeEventListener("click", buttonListener);
            closeModal.removeEventListener("click", closeListener);
            [healthElement, attackElement, defenseElement].forEach(item => {
                item.addEventListener("input", this.changeToMaxValue);
            });
            handleClick(
                event,
                {
                    ...fighter,
                    health: healthElement.input.value,
                    attack: attackElement.input.value,
                    defense: defenseElement.input.value
                },
                false
            );
            this.closeWrapper();
        });

        closeListener = closeModal.addEventListener(
            "click",
            event => {
                buttonElement.removeEventListener("click", buttonListener);
                closeModal.removeEventListener("click", closeListener);
                this.closeWrapper(event);
            },
            false
        );
    }



    createName(name) {
        const nameElement = this.createElement({
            tagName: "h3",
            className: "name"
        });
        nameElement.innerText = name;
        return nameElement;
    }

    createInput(name, id, value) {
        const inputAttributes = {
            id: id,
            type: "number",
            title: "from 1 to 100 please",
            min: "1",
            max: "100",
            autocomplete: "on",
            pattern: "^d{0,3}$",
            required: true
        };
        const labelAttributes = {
            for: id
        };
        const wrapperElement = this.createElement({
            tagName: "div",
            className: "input-wrapper"
        });
        const inputElement = this.createElement({
            tagName: "input",
            className: "input-origin",
            attributes: inputAttributes
        });
        const labelElement = this.createElement({
            tagName: "label",
            className: "input-label",
            attributes: labelAttributes
        });
        inputElement.value = value;
        labelElement.innerText = `${name}: `;
        wrapperElement.append(labelElement, inputElement);
        wrapperElement.input = inputElement;
        return wrapperElement;
    }


    closeWrapper() {
        if (this.element.parentNode) {
            this.element.parentNode.removeChild(this.element);
        }
    }

    createButton(name) {
        const buttonElement = this.createElement({
            tagName: "button",
            className: "button"
        });
        buttonElement.innerText = name;
        return buttonElement;
    }
}

export default Modal;
