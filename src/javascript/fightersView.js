import View from './view';
import FighterView from './fighterView';
import {fighterService} from './services/fightersService';
import Modal from "./modal";
import Fighter from "./fighter";
import {CONSTS} from "./helpers/apiHelper";

class FightersView extends View {
    constructor(fighters) {
        super();

        this.handleFighterClick = this.handleFighterClick.bind(this);
        this.handleModalChooseClick = this.handleModalChooseClick.bind(this);
        this.createFighters(fighters, this.handleFighterClick);
    }

    fightersDetailsMap = new Map();
    fightersMap = new Map();

    createFighters(fighters, handleClick) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter) {
        if (!this.fightersDetailsMap.has(fighter._id)) {
            const fighterDetails = await fighterService.getFighterDetails(
                fighter._id
            );
            this.fightersDetailsMap.set(fighter._id, fighterDetails);
        }
        const fighterMap = this.fightersDetailsMap.get(fighter._id);
        if (this.fightersMap.size >= CONSTS.fightersAmount) {
            const secondFighterId = Array.from(this.fightersMap.keys()).filter(
                id => +id !== +fighter._id
            )[0];
            const firstFighterElement = this.element.querySelector(
                `[id="${fighter._id}"]`
            );
            const secondFighterElement = this.element.querySelector(
                `[id="${secondFighterId}"]`
            );

            secondFighterElement.classList.add("defence");
            const coords = firstFighterElement.getBoundingClientRect();

            if (document.documentElement.clientWidth < coords.left + coords.right) {
                firstFighterElement.classList.add("attackLeft");
            } else {
                firstFighterElement.classList.add("attackRight");
            }

            setTimeout(() => {
                firstFighterElement.classList.remove("attackLeft", "attackRight");
            }, CONSTS.attackDelay);

            setTimeout(() => {
                secondFighterElement.classList.remove("defence");
            }, CONSTS.defenceDelay);
            if (this.fightersMap.has(fighter._id)) {
                fighterService.fight(
                    this.fightersMap.get(fighter._id),
                    this.fightersMap.get(secondFighterId)
                );
            }
        } else {
            const showInfo = (fighterService.getFighterDetails(fighterMap._id))
                .then(showInfo => {
                    const modal = new Modal(
                        this.fightersDetailsMap.get(fighter._id),
                        this.handleModalChooseClick
                    );
                    this.element.appendChild(modal.element);

                });

        }
    }

    handleModalChooseClick(event, fighter) {
        const {_id, name, health, attack, defense} = fighter;
        this.fightersDetailsMap.set(_id, fighter);
        this.fighter = new Fighter(name, health, attack, defense);
        this.fightersMap.set(_id, this.fighter);
        this.element
            .querySelector(`[id="${_id}"]`)
            .classList.add("chosen");
        if (this.fightersMap.size >= CONSTS.fightersAmount) {
            const fighterElements = this.element.querySelectorAll(".fighter");
            if (fighterElements.length > CONSTS.fightersAmount) {
                const fightersArray = Array.from(this.fightersMap.keys());
                const [firstFighterId, secondFighterId] = fightersArray;
                fighterElements.forEach(item => {
                    if (+item.id !== +firstFighterId && +item.id !== +secondFighterId) {
                        item.classList.add("hidden");
                    } else {
                        item.classList.remove("chosen");
                    }
                });
                if (!this.element.querySelector(".rotate")) {
                    if (firstFighterId > secondFighterId) {
                        this.element
                            .querySelector(`[id="${firstFighterId}"] .fighter-image`)
                            .classList.add("rotate")
                    } else {
                        this.element
                            .querySelector(`[id="${secondFighterId}"] .fighter-image`)
                            .classList.add("rotate");
                    }
                }
            }
        }
    }

}

export default FightersView;