class Fighter {
    constructor(name, health, attack, defense) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.getHitPower = this.getHitPower.bind(this);
        this.getBlockPower = this.getBlockPower.bind(this);
    }

    getHitPower() {
        let criticalHitChance = this.getChance();
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        let dodgeChance = this.getChance();
        return this.defense * dodgeChance;
    }
    getHealth() {
        return this.health;
    }

    setHealth(health) {
        this.health = health;
    }

    getAttack() {
        return this.attack;
    }

    setAttack(attack) {
        this.attack = attack;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getDefense() {
        return this.defense;
    }

    setDefense(defense) {
        this.defense = defense;
    }

    getChance(){
        return Math.random() * 2 + 1;
    }
}

export default Fighter;